#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
	if (argc < 2 || argc > 3) {
		fputs("Invalid parameters.\n", stdout);
		return 1;
	}
	if (access(argv[1], F_OK) != 0) {

		FILE * fp;
		fp = fopen(argv[1], "w+");
		
		if (fp != NULL) {
			fputs("File created.\n", stdout);
			if (argc == 3) {
				fputs(argv[2], fp);
			}
			fclose(fp);
			return 0;
		}
		else {
			fputs("File creation failed.\n", stdout);
			return 1;
		}
	}
	else {
		fputs("File exists.\n", stdout);
		return 0;
	}
}
