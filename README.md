Checks if a file name of the first parameter exists in the same directory. 
If the file doesn't exist, the program will create a text file of the name.
If a third parameter is added, a text file will be initialized with the text in the third parameter.

1. Compile from the directory with: gcc -o file_checker file_check_create.c
2. Test: ./file_checker "hamster" "the home of a happy little hamster"
